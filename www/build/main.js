webpackJsonp([9],{

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetallesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_carro_carro__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_producto_producto__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pago_pago__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the DetallesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetallesPage = /** @class */ (function () {
    function DetallesPage(navCtrl, navParams, _producto, _loading, ShopCar, _toast, _productoService, _modalController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._producto = _producto;
        this._loading = _loading;
        this.ShopCar = ShopCar;
        this._toast = _toast;
        this._productoService = _productoService;
        this._modalController = _modalController;
        this.productos = []; //with filter
        this.ProductosApi = []; //without filter
        this.ProductosApiFilter = []; //in the filtering
        this.id = navParams.get("id");
        console.log("id enviado ", this.id);
    }
    DetallesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad DetallesPage');
        var loading = this._loading.create({
            content: "Please wait...",
        });
        loading.present();
        console.log("Loading creado");
        var observable = this._productoService.getProductosApi();
        console.log("observable del detalles");
        observable.subscribe(function (result) {
            _this.ProductosApi = result;
            _this.productos = _this.ProductosApi;
            _this.ProductosApiFilter = _this.ProductosApi;
            filtrado();
            loading.dismiss();
            console.log("Cerrando el loading y filtrado completado");
            console.table(_this.productos);
        });
        var self = this;
        function filtrado() {
            if (self.id != "15") {
                console.log(self.ProductosApi);
                console.log("estas dentro del condicional id<>15");
                console.table("arreglo antes de ser filtrado: " + self.ProductosApi);
                self.productos = self.ProductosApiFilter.filter(function (product) {
                    return product.categories[0].id == parseInt(self.id);
                });
                console.log("before of:" + self.productos[0]);
            }
            else {
                self.productos = self.ProductosApi;
            }
        }
    };
    DetallesPage.prototype.addToShopCar = function (name, id, $, img) {
        console.log("nombre", name, "id", id, "precio: ", $, "imagen: ", img);
        this.ShopCar.agregarAlCarrito(name, id, parseInt($), img);
        console.log(this.ShopCar);
        if (this.ShopCar.almacen.length > 0) {
            this.mostrarToast("¡producto Agregado a tu carrito!");
        }
    };
    DetallesPage.prototype.mostrarToast = function (mensaje) {
        var toast = this._toast.create({
            message: mensaje,
            duration: 3000,
            position: 'middle'
        });
        toast.present();
    };
    DetallesPage.prototype.irPago = function () {
        var modal = this._modalController.create(__WEBPACK_IMPORTED_MODULE_4__pago_pago__["a" /* PagoPage */], { "precio": this.productos[0].precio });
        modal.present();
    };
    DetallesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-detalles',template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\detalles\detalles.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title *ngIf="productos.length > 0">{{productos[0].name}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding *ngIf="productos.length > 0">\n  <section>\n		<h2 id="Encabezado">Titulo de la publicacion</h2>\n</section>\n	<div class="container-fluid">\n\n		<div class="row">\n\n			<div class="col-xs-12 col-md-4 col-md-offset-1">\n				<h1 class="h-center">{{productos[0].name}}</h1>\n					<div id="carousel-example-generic" class="carousel slide xs-hidden" data-ride="carousel">\n						<!-- Indicators -->\n						<ol class="carousel-indicators">\n							<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>\n							<li data-target="#carousel-example-generic" data-slide-to="1"></li>\n							<li data-target="#carousel-example-generic" data-slide-to="2"></li>\n						</ol>\n					\n						<!-- Wrapper for slides -->\n						<div class="carousel-inner" role="listbox">\n							<div class="item active">\n								<img [src]="productos[0].images[0].src" alt="foto1" title="productos[0].images[0].name" class="img-responsive carrusel_img">\n								<div class="carousel-caption">\n								...\n							</div>\n							</div>\n							<div class="item">\n								<img [src]="productos[0].images[0].src" alt="foto2" title="productos[0].images[0].name" class="img-responsive carrusel_img">\n								<div class="carousel-caption">\n									<h3>DEMECIA!</h3>\n									<p>Exelente material,labia,labia,labia...</p>\n								</div>\n							</div>\n							<div class="item">\n									<img [src]="productos[0].images[0].src" alt="foto3" title="productos[0].images[0].name" class="img-responsive carrusel_img">\n									<div class="carousel-caption">\n										...\n									</div>\n								</div>\n						</div>\n					\n						<!-- Controls -->\n						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">\n							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>\n							<span class="sr-only">Previous</span>\n						</a>\n						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">\n							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>\n							<span class="sr-only">Next</span>\n						</a>\n					</div>\n\n					<a (click)=irPago() class="btn btn-primary btn-sm buyBottoms"><span class="glyphicon glyphicon-road"></span>Comprar</a>\n					<button (click)="addToShopCar(productos[0].name,productos[0].id,productos[0].price,productos[0].images[0].src)" class="btn btn-primary btn-sm buyBottoms"><span class="glyphicon glyphicon-road"></span>Agregar al carrito</button>\n\n			</div>\n			<div class="row">\n				<div class="col-xs-10 col-xs-offset-2 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">\n					<h2 class="h-center">\n						<strong>Detalles</strong>\n					</h2>\n					<div id="perfil-detalles">\n						<p>{{productos[0].description}} </p>\n					</div>\n				</div>\n			</div>\n			<div class="row">\n				<div class="col-xs-12 col-md-8 col-md-offset-1">\n					<h2 class="h-center">\n						Comentarios\n					</h2>\n					<form>\n							<textarea class="form-control" rows="3" id="comentarios"></textarea>\n							<button type="submit" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-road"></span>Enviar</button>\n					</form>\n				</div>\n			</div>\n		</div>\n\n	</div>\n\n</ion-content>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\detalles\detalles.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__providers_producto_producto__["a" /* ProductoProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_0__providers_carro_carro__["a" /* CarroProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1__providers_producto_producto__["a" /* ProductoProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* ModalController */]])
    ], DetallesPage);
    return DetallesPage;
}());

//# sourceMappingURL=detalles.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContraseñaOlvidadaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_cuentas__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContraseñaOlvidadaPage = /** @class */ (function () {
    function ContraseñaOlvidadaPage(navCtrl, navParams, CuentasProvider, ToastController, viewCrtl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.CuentasProvider = CuentasProvider;
        this.ToastController = ToastController;
        this.viewCrtl = viewCrtl;
        this.correo = "";
        this.clave = "";
    }
    ContraseñaOlvidadaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContraseñaOlvidadaPage');
    };
    ContraseñaOlvidadaPage.prototype.mostrarToast = function (mensaje) {
        var toast = this.ToastController.create({
            message: mensaje,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    ContraseñaOlvidadaPage.prototype.buscarUsuario = function () {
        var encontrado = this.CuentasProvider.buscarPorCorreo(this.correo);
        if (encontrado > -1) {
            this.mostrarToast("Contrasea encontrada!");
            this.clave = this.CuentasProvider.cuentas[encontrado].clave;
        }
        else {
            this.mostrarToast("El correo ingresado no existe en nuestro sistema");
        }
    };
    ContraseñaOlvidadaPage.prototype.cerrar = function () {
        this.viewCrtl.dismiss();
    };
    ContraseñaOlvidadaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contraseña-olvidada',template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\contraseña-olvidada\contraseña-olvidada.html"*/'<!--\n  Generated template for the ContraseñaOlvidadaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>contraseñaOlvidada</ion-title>\n    <ion-buttons end>\n      <button ion-button (click)="cerrar()">\n        <span ion-text color="primary" showWhen="ios">Close</span>\n        <ion-icon name="close"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <ion-icon name="person" item-start></ion-icon>\n      <ion-label stacked>Correo:</ion-label>\n      <ion-input type="text" placeholder="Correo electronico" [(ngModel)]="correo"></ion-input>\n    </ion-item>\n    <ion-item *ngIf="clave!==\'\' ">\n      <ion-icon name="cloudy-night" item-start></ion-icon>\n      <ion-label stacked>Contraseña:</ion-label>\n      <ion-input type="text" placeholder="Contraseña" [(ngModel)]="clave"></ion-input>\n    </ion-item>\n    <button ion-button block type="button" (click)="buscarUsuario()">Buscar Clave</button>\n    \n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\contraseña-olvidada\contraseña-olvidada.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_cuentas__["a" /* CuentasProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
    ], ContraseñaOlvidadaPage);
    return ContraseñaOlvidadaPage;
}());

//# sourceMappingURL=contraseña-olvidada.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActualizarDatosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_cuentas__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ActualizarDatosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ActualizarDatosPage = /** @class */ (function () {
    function ActualizarDatosPage(navCtrl, navParams, toast, cuentasProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.cuentasProvider = cuentasProvider;
        this.nombrecompleto = "";
        this.correo = "";
        this.nombreUsuario = "";
        this.clave = "";
        this.fecha = "";
        this.usuario = this.navParams.get("usuario");
        console.log(this.usuario);
        this.nombrecompleto = this.usuario.nombreCompleto;
        this.correo = this.usuario.correo;
        this.nombreUsuario = this.usuario.nombreUsuario;
        this.clave = this.usuario.clave;
        this.fecha = this.usuario.fechaDeNacimiento;
    }
    ActualizarDatosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ActualizarDatosPage');
        this.posicionUsuario = this.cuentasProvider.buscarPorUsuario(this.nombreUsuario);
    };
    ActualizarDatosPage.prototype.mostrarToast = function (mensaje) {
        var toast = this.toast.create({
            message: mensaje,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    ActualizarDatosPage.prototype.Actualizar = function () {
        if (this.validarCampos()) {
            this.mostrarToast("Actualizacion de datos exitosa!");
            this.cuentasProvider.cuentas[this.posicionUsuario].nombreCompleto = this.nombrecompleto;
            this.cuentasProvider.cuentas[this.posicionUsuario].correo = this.correo;
            this.cuentasProvider.cuentas[this.posicionUsuario].nombreUsuario = this.nombreUsuario;
            this.cuentasProvider.cuentas[this.posicionUsuario].clave = this.clave;
            this.cuentasProvider.cuentas[this.posicionUsuario].fechaDeNacimiento = this.fecha;
            console.log(this.cuentasProvider.cuentas[this.posicionUsuario]);
            console.log(this.usuario);
        }
    };
    ActualizarDatosPage.prototype.validarCampos = function () {
        if (this.nombrecompleto == "" || this.nombrecompleto.length < 8) {
            this.mostrarToast("Debe ingresar su nombre completo");
            return false;
        }
        else if (this.correo == "") {
            this.mostrarToast("Debe ingresar un correo electronico");
            return false;
        }
        else if (this.nombreUsuario == "") {
            this.mostrarToast("Debe ingresar un nombre de usuario");
            return false;
        }
        else if (this.clave == "") {
            this.mostrarToast("Debe ingresar una clave");
            return false;
        }
        else if (this.fecha == "") {
            this.mostrarToast("Debe ingresar una fecha de nacimiento");
            return false;
        }
        else
            return true;
    };
    ActualizarDatosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-actualizar-datos',template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\actualizar-datos\actualizar-datos.html"*/'<!--\n  Generated template for the ActualizarDatosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>actualizarDatos</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <ion-icon name="person-add" item-start></ion-icon>\n      <ion-label stacked>Nombre completo:</ion-label>\n      <ion-input type="text" placeholder="Nombre" [(ngModel)]="nombrecompleto"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-icon name="person-add" item-start></ion-icon>\n      <ion-label stacked>Nombre de usuario:</ion-label>\n      <ion-input type="text" placeholder="Nombre de usuario" [(ngModel)]="nombreUsuario"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-icon name="mail" item-start></ion-icon>\n      <ion-label stacked>Correo electronico:</ion-label>\n      <ion-input type="email" placeholder="Email" [(ngModel)]="correo"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-icon name="calendar" item-start></ion-icon>\n      <ion-label stacked>Fecha de nacimiento:</ion-label>\n      <ion-input  displayFormat="MM-DD-YYYY" placeholder="MM-DD-YYY" [(ngModel)]="fecha"></ion-input>\n    </ion-item>\n      <ion-item>\n        <ion-icon name="eye" item-start></ion-icon>\n        <ion-label stacked>Contraseña:</ion-label>\n        <ion-input type="password" placeholder="Contraseña" [(ngModel)]="clave"></ion-input>\n      </ion-item>\n  </ion-list>\n<div padding>\n  <button ion-button block type="button" (click)="Actualizar()">Actualizar</button>\n</div>\n</ion-content>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\actualizar-datos\actualizar-datos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_cuentas__["a" /* CuentasProvider */]])
    ], ActualizarDatosPage);
    return ActualizarDatosPage;
}());

//# sourceMappingURL=actualizar-datos.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__carro_d_compras_carro_d_compras__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_producto_producto__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__inicio_sesion_inicio_sesion__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__actualizar_datos_actualizar_datos__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__detalles_detalles__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, _productosApi, _loading, _toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._productosApi = _productosApi;
        this._loading = _loading;
        this._toast = _toast;
        this.n = 0;
        this.categoriasApi = [];
        this.usuario = this.navParams.get("usuario");
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad HomePage');
        var loading = this._loading.create({
            content: "Please wait...",
        });
        loading.present();
        var observable = this._productosApi.getCategoriasAPI();
        observable.subscribe(function (result) {
            _this.categoriasApi = result;
            loading.dismiss();
        });
    };
    HomePage.prototype.cerrarSesion = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__inicio_sesion_inicio_sesion__["a" /* InicioSesionPage */]);
    };
    HomePage.prototype.actualizar = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__actualizar_datos_actualizar_datos__["a" /* ActualizarDatosPage */], { "usuario": this.usuario });
    };
    HomePage.prototype.verDetalle = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__detalles_detalles__["a" /* DetallesPage */], { "id": id });
    };
    HomePage.prototype.Vercarro = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__carro_d_compras_carro_d_compras__["a" /* CarroDComprasPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\home\home.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{usuario.nombreCompleto}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-item-group>\n		<ion-item-divider color="dark">\n			Bienvenido de nuevo {{usuario.nombreUsuario}}\n		</ion-item-divider>\n	</ion-item-group>  \n  <div padding>\n    <div *ngIf="categoriasApi.length>0">\n      <button (click)="verDetalle(opcion.id)" *ngFor="let opcion of categoriasApi" ion-button block type="button">{{opcion.name}}</button>\n\n    </div>\n    <ion-row>\n      <ion-col>\n          <button ion-button block type="button" (click)="actualizar()">Actualizar datos</button>\n      </ion-col>\n      <ion-col>\n        <button ion-button block type="button" (click)="Vercarro()">Carrito</button>\n    </ion-col>\n      <ion-col>\n          <button ion-button block type="button" (click)="cerrarSesion()">Cerrar Sesion</button>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\home\home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__providers_producto_producto__["a" /* ProductoProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* ToastController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarroDComprasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_carro_carro__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pago_pago__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CarroDComprasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CarroDComprasPage = /** @class */ (function () {
    function CarroDComprasPage(navCtrl, navParams, _almacen, modal) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._almacen = _almacen;
        this.modal = modal;
        this.almacen = [];
    }
    CarroDComprasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CarroDComprasPage');
        this.almacen = this._almacen.almacen;
        console.log(this.cantidad);
        if (this._almacen.almacen.length > 0) {
            this.total = this.cantidad * this._almacen.almacen[0].precio;
        }
    };
    CarroDComprasPage.prototype.eliminarDelCarro = function (id) {
        this._almacen.eliminarDelCarrito(id);
    };
    CarroDComprasPage.prototype.pagar = function () {
        var modal = this.modal.create(__WEBPACK_IMPORTED_MODULE_3__pago_pago__["a" /* PagoPage */]);
        modal.present();
    };
    CarroDComprasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-carro-d-compras',template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\carro-d-compras\carro-d-compras.html"*/'<!--\n  Generated template for the CarroDComprasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>carroDCompras</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  \n  <div class="table-responsive col-md-12" *ngIf ="almacen.length > 0">\n    <table class="table" >\n        <!--Fila de los titulos-->\n        <tr>\n            <th></th>\n            <th>Imagen</th>\n            <th>Precio</th>\n            <th>Cantidad</th>\n            <th>Codigo</th>\n            <th>Total</th>					    \n        </tr>\n        <!--Contenidos-->\n        <tr *ngFor="let producto of almacen">\n            <th width="62px"><button type="button" (click)="eliminarDelCarro(producto.id)" class="btn btn-danger btn-sm">Eliminar <span class="glyphicon glyphicon-remove"></span></button></th>\n            <th><img src="{{producto.img}}">\n            </th>\n            <th>{{producto.precio}}$</th>\n            <th width="62px"><input type="number" name="cantidad" placeholder="CANTIDAD" class="btn" [(ngModel)]="cantidad"></th>\n            <th width="62px">{{producto.id}}</th>\n            <th>{{producto.facturar()}}$</th>\n        </tr>\n        <button ion-button slot="end" (click)=pagar()>\n          Pagar \n        </button>\n    </table>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\carro-d-compras\carro-d-compras.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_carro_carro__["a" /* CarroProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */]])
    ], CarroDComprasPage);
    return CarroDComprasPage;
}());

//# sourceMappingURL=carro-d-compras.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_cuentas__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegistroPage = /** @class */ (function () {
    function RegistroPage(navCtrl, navParams, cuentasProvider, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.cuentasProvider = cuentasProvider;
        this.toast = toast;
        this.nombrecompleto = "";
        this.correo = "";
        this.nombreUsuario = "";
        this.clave = "";
        this.fecha = "";
    }
    RegistroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegistroPage');
    };
    RegistroPage.prototype.validarExistencia = function () {
        if (this.validarCampos()) {
            var existente = this.cuentasProvider.buscarPorCorreo(this.correo);
            if (existente > -1) {
                this.mostrarToast("Este correo ya se encuentra registrado");
            }
            else if (this.cuentasProvider.buscarPorUsuario(this.nombreUsuario) > -1) {
                this.mostrarToast("Este nombre de usuario se encuentra en uso");
            }
            else {
                this.mostrarToast("Registro exitoso");
                this.cuentasProvider.agregarCuenta(this.nombrecompleto.toLocaleLowerCase(), this.correo, this.nombreUsuario, this.clave, this.fecha);
            }
        }
    };
    RegistroPage.prototype.validarCampos = function () {
        if (this.nombrecompleto == "" || this.nombrecompleto.length < 8) {
            this.mostrarToast("Debe ingresar su nombre completo");
            return false;
        }
        else if (this.correo == "") {
            this.mostrarToast("Debe ingresar un correo electronico");
            return false;
        }
        else if (this.nombreUsuario == "") {
            this.mostrarToast("Debe ingresar un nombre de usuario");
            return false;
        }
        else if (this.clave == "") {
            this.mostrarToast("Debe ingresar una clave");
            return false;
        }
        else if (this.fecha == "") {
            this.mostrarToast("Debe ingresar una fecha de nacimiento");
            return false;
        }
        else
            return true;
    };
    RegistroPage.prototype.mostrarToast = function (mensaje) {
        var toast = this.toast.create({
            message: mensaje,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    RegistroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-registro',template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\registro\registro.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Formulario</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n      <ion-list>\n        <ion-item>\n          <ion-icon name="person-add" item-start></ion-icon>\n          <ion-label stacked>Nombre completo:</ion-label>\n          <ion-input type="text" placeholder="Nombre" [(ngModel)]="nombrecompleto"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="person-add" item-start></ion-icon>\n          <ion-label stacked>Nombre de usuario:</ion-label>\n          <ion-input type="text" placeholder="Nombre de usuario" [(ngModel)]="nombreUsuario"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="mail" item-start></ion-icon>\n          <ion-label stacked>Correo electronico:</ion-label>\n          <ion-input type="email" placeholder="Email" [(ngModel)]="correo"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="calendar" item-start></ion-icon>\n          <ion-label stacked>Fecha de nacimiento:</ion-label>\n          <ion-input  displayFormat="MM-DD-YYYY" placeholder="MM-DD-YYY" [(ngModel)]="fecha"></ion-input>\n        </ion-item>\n          <ion-item>\n            <ion-icon name="eye" item-start></ion-icon>\n            <ion-label stacked>Contraseña:</ion-label>\n            <ion-input type="password" placeholder="Contraseña" [(ngModel)]="clave"></ion-input>\n          </ion-item>\n      </ion-list>\n    <div padding>\n      <button ion-button block type="button" (click)="validarExistencia()">Registrarse</button>\n    </div>\n</ion-content>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\registro\registro.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_cuentas__["a" /* CuentasProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]])
    ], RegistroPage);
    return RegistroPage;
}());

//# sourceMappingURL=registro.js.map

/***/ }),

/***/ 121:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 121;

/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/actualizar-datos/actualizar-datos.module": [
		286,
		8
	],
	"../pages/carro-d-compras/carro-d-compras.module": [
		291,
		7
	],
	"../pages/contraseña-olvidada/contraseña-olvidada.module": [
		285,
		6
	],
	"../pages/detalles/detalles.module": [
		288,
		5
	],
	"../pages/home/home.module": [
		287,
		4
	],
	"../pages/inicio-sesion/inicio-sesion.module": [
		293,
		3
	],
	"../pages/pago/pago.module": [
		290,
		2
	],
	"../pages/productos/productos.module": [
		289,
		0
	],
	"../pages/registro/registro.module": [
		292,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 164;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__inicio_sesion_inicio_sesion__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registro_registro__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__inicio_sesion_inicio_sesion__["a" /* InicioSesionPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__registro_registro__["a" /* RegistroPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\tabs\tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Iniciar Sesion" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Registro" tabIcon="information-circle"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(230);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_detalles_detalles__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_actualizar_datos_actualizar_datos__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_home_home__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_carro_d_compras_carro_d_compras__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_pago_pago__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_inicio_sesion_inicio_sesion__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_registro_registro__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_cuentas_cuentas__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_contrase_a_olvidada_contrase_a_olvidada__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_carro_carro__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_producto_producto__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_registro_registro__["a" /* RegistroPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_inicio_sesion_inicio_sesion__["a" /* InicioSesionPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_contrase_a_olvidada_contrase_a_olvidada__["a" /* ContraseñaOlvidadaPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_actualizar_datos_actualizar_datos__["a" /* ActualizarDatosPage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_detalles_detalles__["a" /* DetallesPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_carro_d_compras_carro_d_compras__["a" /* CarroDComprasPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_pago_pago__["a" /* PagoPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/contraseña-olvidada/contraseña-olvidada.module#ContraseñaOlvidadaPageModule', name: 'ContraseñaOlvidadaPage', segment: 'contraseña-olvidada', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/actualizar-datos/actualizar-datos.module#ActualizarDatosPageModule', name: 'ActualizarDatosPage', segment: 'actualizar-datos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detalles/detalles.module#DetallesPageModule', name: 'DetallesPage', segment: 'detalles', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/productos/productos.module#ProductosPageModule', name: 'ProductosPage', segment: 'productos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pago/pago.module#PagoPageModule', name: 'PagoPage', segment: 'pago', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/carro-d-compras/carro-d-compras.module#CarroDComprasPageModule', name: 'CarroDComprasPage', segment: 'carro-d-compras', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/registro/registro.module#RegistroPageModule', name: 'RegistroPage', segment: 'registro', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/inicio-sesion/inicio-sesion.module#InicioSesionPageModule', name: 'InicioSesionPage', segment: 'inicio-sesion', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_registro_registro__["a" /* RegistroPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_inicio_sesion_inicio_sesion__["a" /* InicioSesionPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_contrase_a_olvidada_contrase_a_olvidada__["a" /* ContraseñaOlvidadaPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_actualizar_datos_actualizar_datos__["a" /* ActualizarDatosPage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_detalles_detalles__["a" /* DetallesPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_carro_d_compras_carro_d_compras__["a" /* CarroDComprasPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_pago_pago__["a" /* PagoPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["a" /* HttpClient */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_3__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_15__providers_cuentas_cuentas__["a" /* CuentasProvider */],
                __WEBPACK_IMPORTED_MODULE_17__providers_carro_carro__["a" /* CarroProvider */],
                __WEBPACK_IMPORTED_MODULE_18__providers_producto_producto__["a" /* ProductoProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(208);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CuentasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { HttpClient } from '@angular/common/http';

var CuentasProvider = /** @class */ (function () {
    function CuentasProvider() {
        this.cuentas = [{
                nombreCompleto: "Anderson Gil",
                correo: "anderson111903@ionic.com",
                nombreUsuario: "p",
                clave: "1",
                fechaDeNacimiento: "10/20/2000"
            }];
        console.log('Hello CuentasProvider Provider');
    }
    CuentasProvider.prototype.agregarCuenta = function (nc, co, nu, cla, f) {
        this.cuentas.push({
            nombreCompleto: nc,
            correo: co,
            nombreUsuario: nu,
            clave: cla,
            fechaDeNacimiento: f
        });
    };
    CuentasProvider.prototype.buscarPorCorreo = function (correo) {
        var resultado = this.cuentas.findIndex(function (usuario) { return usuario.correo === correo; });
        return resultado;
    };
    CuentasProvider.prototype.buscarPorUsuario = function (userName) {
        var resultado = this.cuentas.findIndex(function (usuario) { return usuario.nombreUsuario === userName; });
        return resultado;
    };
    CuentasProvider.prototype.ValidarEntrada = function (userName, clave) {
        var resultado = this.cuentas.findIndex(function (usuario) { return usuario.nombreUsuario === userName; });
        if (resultado > -1) {
            if (this.cuentas[resultado].clave === clave) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    CuentasProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], CuentasProvider);
    return CuentasProvider;
}());

var Usuario = /** @class */ (function () {
    function Usuario(nc, co, nu, cla, f) {
        this.nombreCompleto = nc;
        this.correo = co;
        this.nombreUsuario = nu;
        this.clave = cla;
        this.fechaDeNacimiento = f;
    }
    return Usuario;
}());
//# sourceMappingURL=cuentas.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarroProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { HttpClient } from '@angular/common/http';

/*
  Generated class for the CarroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var CarroProvider = /** @class */ (function () {
    //productos:Producto [] = [] ;
    function CarroProvider() {
        this.almacen = [];
        console.log("haz llamado a cuentas provider, y estos son los productos agregados hasta el momento", +this.almacen);
    }
    CarroProvider.prototype.agregarAlCarrito = function (name, id, price, img, ca) {
        this.almacen.push(new Shopcar(name, id, price, img, ca));
        //{
        //forma en tipo json            
        //nombre        : name,
        //id            : id,
        //precio        : price,
        //img           : img,
        //cantidad      : ca
        //}
    };
    /*    public agregarCarrito(name:string,price:number,ca:number,sub?:number){
        this.productos.push({
          nombre        : name,
          precio        : price,
          cantidad      : ca,
          subtotal      : sub,
        
        });
       } */
    CarroProvider.prototype.eliminarDelCarrito = function (id) {
        var posicion = this.almacen.findIndex(function (producto) { return producto.id == id; });
        if (posicion > -1) {
            this.almacen.splice(posicion, 1);
        }
    };
    CarroProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], CarroProvider);
    return CarroProvider;
}());

var Shopcar = /** @class */ (function () {
    function Shopcar(name, id, price, img, ca) {
        this.nombre = name;
        this.id = id;
        this.precio = price;
        this.img = img;
        this.cantidad = ca;
        if (this.cantidad == undefined) {
            this.cantidad = 1;
        }
        else {
            this.cantidad = ca;
        }
    }
    Shopcar.prototype.facturar = function () {
        var factura = this.precio * this.cantidad;
        return factura;
    };
    return Shopcar;
}());
/* class Producto{

  nombre:string
  precio:number
  cantidad:number
  subtotal:number

  constructor(nombre:string,precio:number,cantidad:number,sub?:number)
    {
      this.nombre=nombre;
      this.precio=precio;
      this.cantidad=cantidad;
      this.subtotal=sub;
      this.subtotal= this.precio*this.cantidad;

    }

    facturar(){
      let factura=this.precio*this.cantidad;
      return factura
    }

} */
//# sourceMappingURL=carro.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_carro_carro__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PagoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PagoPage = /** @class */ (function () {
    function PagoPage(navCtrl, navParams, _toast, almacen, viewCrtl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._toast = _toast;
        this.almacen = almacen;
        this.viewCrtl = viewCrtl;
        this.cedula = "";
        this.nombre = "";
        this.nombreUsuario = "";
        this.claveTarjeta = "";
        this.mesDVencimiento = "";
        this.tipoDCuenta = "";
        this.precio = "";
        this.precio = this.navParams.get("precio");
    }
    PagoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PagoPage');
        console.log("precioTotal:" + this.precio);
    };
    PagoPage.prototype.comprar = function () {
        this.mostrarToast("compra exitosa");
        this.almacen.almacen.splice(0, this.almacen.almacen.length);
    };
    PagoPage.prototype.cerrar = function () {
        this.viewCrtl.dismiss();
    };
    PagoPage.prototype.mostrarToast = function (mensaje) {
        var toast = this._toast.create({
            message: mensaje,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    PagoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pago',template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\pago\pago.html"*/'<!--\n  Generated template for the PagoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>pago</ion-title>\n    <ion-buttons end>\n        <button ion-button (click)="cerrar()">\n          <span ion-text color="primary" showWhen="ios">Close</span>\n          <ion-icon name="close"></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <section>\n        <h2 id="Encabezado">Procesamiento de pago</h2> \n        <span class="glyphicon glyphicon-credit-card" id="glyphicon-credit-card"></span>\n    </section>\n\n    	<form  #formulario="ngForm">\n               <ion-list id="formulario">\n                 <ion-item >\n                   <ion-label stacked>\n                     TOTAL: {{precio}}\n                   </ion-label>\n                 </ion-item>\n                  <ion-item>\n                    <ion-label stacked>Cedula:</ion-label>\n                    <ion-input type="text" name="cedula" placeholder="EJ:28119876" maxlength="8" [(ngModel)]="cedula" required></ion-input>\n                  </ion-item>\n\n                  <ion-item>\n                    <ion-label stacked>Nombre:</ion-label>\n                    <ion-input type="text" name="nombre" placeholder="EJ:ingrese su nombre" id="nombre"[(ngModel)]="nombre" required></ion-input>\n                  </ion-item>\n\n                  <ion-item>\n                    <ion-label stacked>Nombre de usuario:</ion-label>\n                    <ion-input type="text" name="Nusuario" placeholder="Ej:maclovin nombredeusuario"[(ngModel)]="nombreUsuario" required></ion-input>\n                  </ion-item>\n\n                  <ion-item>\n                    <ion-label stacked>Clave De la tarjeta:</ion-label>\n                    <ion-input type="text" type="password" name="clave de la tarjeta" placeholder="minimo 4 caracteres" required [(ngModel)]="claveTarjeta"></ion-input>\n                  </ion-item>\n\n                  <ion-item>\n                    <ion-label stacked>Mes De Vencimiento:</ion-label>\n                    <ion-input type="text" name="nombre" placeholder="EJ:mes de vecimiento de la tarjeta 01/10/20" [(ngModel)]="mesDVencimiento" required></ion-input>\n                  </ion-item>\n\n                  <ion-item>\n                    <ion-label stacked>Tipo De Cuenta:</ion-label>\n                    <ion-input type="text" name="nombre" placeholder="EJ:ingrese el tipo de cuenta" id="nombre"[(ngModel)]="tipoDCuenta" required></ion-input>\n                  </ion-item>\n              </ion-list>\n\n              <button [disabled]="!formulario.valid" ion-button block type="button" (click)="comprar()">Comprar</button>\n      </form>\n\n</ion-content>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\pago\pago.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__providers_carro_carro__["a" /* CarroProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ViewController */]])
    ], PagoPage);
    return PagoPage;
}());

//# sourceMappingURL=pago.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InicioSesionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_cuentas__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__contrase_a_olvidada_contrase_a_olvidada__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InicioSesionPage = /** @class */ (function () {
    function InicioSesionPage(navCtrl, cuentasProvider, modalController, toast) {
        this.navCtrl = navCtrl;
        this.cuentasProvider = cuentasProvider;
        this.modalController = modalController;
        this.toast = toast;
        this.nombreUsuario = "";
        this.clave = "";
    }
    InicioSesionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InicioSesionPage');
    };
    InicioSesionPage.prototype.validarUsuario = function () {
        if (this.nombreUsuario != "" || this.clave != "") {
            if (this.cuentasProvider.ValidarEntrada(this.nombreUsuario, this.clave)) {
                this.mostrarToast("Iniciando..");
                var usuario = this.cuentasProvider.buscarPorUsuario(this.nombreUsuario);
                this.nombreUsuario = "";
                this.clave = "";
                console.log(this.cuentasProvider.cuentas[usuario]);
                this.irHome(usuario);
            }
            else
                this.mostrarToast("Nombre De Usuario o Contraseña Incorrecta");
        }
    };
    InicioSesionPage.prototype.irHome = function (usuario) {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */], { "usuario": this.cuentasProvider.cuentas[usuario] });
    };
    InicioSesionPage.prototype.Irperfil = function () {
        var modal = this.modalController.create(__WEBPACK_IMPORTED_MODULE_4__contrase_a_olvidada_contrase_a_olvidada__["a" /* ContraseñaOlvidadaPage */]);
        modal.present();
    };
    InicioSesionPage.prototype.mostrarToast = function (mensaje) {
        var toast = this.toast.create({
            message: mensaje,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    InicioSesionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-inicio-sesion',template:/*ion-inline-start:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\inicio-sesion\inicio-sesion.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>InicioSesion</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <ion-icon name="person" item-start></ion-icon>\n      <ion-label stacked>Nombre de usuario:</ion-label>\n      <ion-input type="text" placeholder="Nombre de Usuario" [(ngModel)]="nombreUsuario"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-icon name="eye" item-start></ion-icon>\n      <ion-label stacked>Contraseña:</ion-label>\n      <ion-input type="password" placeholder="Contraseña" [(ngModel)]="clave"></ion-input>\n    </ion-item>    \n  </ion-list>\n\n  <div padding>\n    <ion-row>\n      <ion-col>\n          <button ion-button block type="button" (click)="validarUsuario()">Iniciar Sesion</button>\n      </ion-col>\n      <ion-col>\n          <button ion-button block type="button" (click)="Irperfil()">Olvidé Mi Contraseña</button>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Mis Respaldos Anderson\Escritorio\Defensa\Proyectoionic\src\pages\inicio-sesion\inicio-sesion.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_cuentas_cuentas__["a" /* CuentasProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]])
    ], InicioSesionPage);
    return InicioSesionPage;
}());

//# sourceMappingURL=inicio-sesion.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the ProductoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ProductoProvider = /** @class */ (function () {
    function ProductoProvider(http) {
        this.http = http;
        console.log('Hello ProductoProvider Provider');
    }
    ProductoProvider.prototype.getCategoriasAPI = function () {
        var url = "https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products/categories?";
        var ck = "consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3";
        var cs = "consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff";
        console.log(this.http.get(url + ck + "&" + cs));
        return this.http.get(url + ck + "&" + cs);
    };
    ProductoProvider.prototype.getProductoById = function (id) {
        var url = "https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products/";
        var ck = "consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3";
        var cs = "consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff";
        return this.http.get(url + id + "?" + ck + "&" + cs);
    };
    ProductoProvider.prototype.getProductosApi = function () {
        var url = "https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products?";
        var ck = "consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3";
        var cs = "consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff";
        return this.http.get(url + ck + "&" + cs);
    };
    ProductoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ProductoProvider);
    return ProductoProvider;
}());

//# sourceMappingURL=producto.js.map

/***/ })

},[209]);
//# sourceMappingURL=main.js.map