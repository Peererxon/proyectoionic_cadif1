//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CarroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CarroProvider {
  almacen :Shopcar[] = [];
  //productos:Producto [] = [] ;
  constructor() {
    console.log("haz llamado a cuentas provider, y estos son los productos agregados hasta el momento",
     + this.almacen);
   }

   public agregarAlCarrito(name:string,id:string,price:number,img:string,ca?:number){
    this.almacen.push( new Shopcar(name,id,price,img,ca) );
          //{
      //forma en tipo json            
      //nombre        : name,
      //id            : id,
      //precio        : price,
      //img           : img,
      //cantidad      : ca
    //}
   }

/*    public agregarCarrito(name:string,price:number,ca:number,sub?:number){
    this.productos.push({
      nombre        : name,
      precio        : price,
      cantidad      : ca,
      subtotal      : sub,
    
    });
   } */
   
   public eliminarDelCarrito(id:string){
     let posicion :number = this.almacen.findIndex(producto => producto.id ==id);
     if(posicion>-1){
      this.almacen.splice(posicion,1);
     }
   }
}

class Shopcar{
  nombre        :string;
  id            :string;
  precio        :number;
  img           :string;
  cantidad      :number;

  constructor(name:string,id:string,price:number,img:string,ca?:number){
    this.nombre        = name;
    this.id            = id;
    this.precio        = price;
    this.img           = img;
    this.cantidad      = ca;
    if(this.cantidad == undefined){
      this.cantidad = 1;
    }else{
      this.cantidad = ca;
    }
 
  }

  facturar(){
    let factura=this.precio*this.cantidad;
    return factura
  } 

}



/* class Producto{

  nombre:string
  precio:number
  cantidad:number
  subtotal:number

  constructor(nombre:string,precio:number,cantidad:number,sub?:number)
    {
      this.nombre=nombre;
      this.precio=precio;
      this.cantidad=cantidad;
      this.subtotal=sub;
      this.subtotal= this.precio*this.cantidad;

    }

    facturar(){
      let factura=this.precio*this.cantidad;
      return factura
    } 

} */

