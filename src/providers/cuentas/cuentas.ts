//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CuentasProvider {
  cuentas:Usuario[]=[{
    nombreCompleto : "Anderson Gil",
    correo:"anderson111903@ionic.com",
    nombreUsuario:"p",
    clave  :"1",
    fechaDeNacimiento: "10/20/2000"
  }];
  constructor(/*public http: HttpClient*/) {
    console.log('Hello CuentasProvider Provider');
  }

 public agregarCuenta(nc:string,co:string,nu:string,cla:string,f:string){
   this.cuentas.push({
     nombreCompleto : nc,
     correo:co,
     nombreUsuario:nu,
     clave  :cla,
     fechaDeNacimiento  : f
   });
  }

  public buscarPorCorreo(correo){
    let resultado=this.cuentas.findIndex(usuario=> usuario.correo===correo);
      return resultado;
  }

  public buscarPorUsuario(userName){
    let resultado=this.cuentas.findIndex(usuario=> usuario.nombreUsuario===userName);
      return resultado;
  }

  public ValidarEntrada(userName,clave){
    let resultado=this.cuentas.findIndex(usuario=> usuario.nombreUsuario===userName);
    if (resultado>-1){
      if (this.cuentas[resultado].clave===clave) {
        return true
        }else{
        return false
        }
    }else{
      return false
    }
  }

 }


class Usuario{
  nombreCompleto:string;
  correo:string;
  nombreUsuario:string;
  clave:string;
  fechaDeNacimiento:string;
    constructor(nc,co,nu,cla,f){
      this.nombreCompleto=nc;
      this.correo=co;
      this.nombreUsuario=nu;
      this.clave=cla;
      this.fechaDeNacimiento=f

    }
}
