import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ProductoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductoProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ProductoProvider Provider');
  }

  public getCategoriasAPI(){
    let url="https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products/categories?";
    let ck="consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3";
    let cs="consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff";

    console.log(this.http.get(url+ck+"&"+cs));

    return this.http.get(url+ck+"&"+cs);
  }

  public getProductoById(id){
    let url="https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products/";
    let ck="consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3";
    let cs="consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff";
    return this.http.get(url+id+"?"+ck+"&"+cs);
  }

  public getProductosApi(){
    let url="https://api-shopcar-cadif1.000webhostapp.com/wp-json/wc/v2/products?";
    let ck="consumer_key=ck_c2d8ae3638be0d856478734a06c701c81bb493f3";
    let cs="consumer_secret=cs_16c5bd4a8bb651e91445c36973e960a5e937a7ff";
    
    return this.http.get(url+ck+"&"+cs);
  }


}
