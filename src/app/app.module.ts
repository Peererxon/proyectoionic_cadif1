import { DetallesPage } from './../pages/detalles/detalles';
import { ActualizarDatosPage } from './../pages/actualizar-datos/actualizar-datos';
import { HomePage } from './../pages/home/home';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CarroDComprasPage } from '../pages/carro-d-compras/carro-d-compras';
import { PagoPage } from '../pages/pago/pago';


import { TabsPage } from '../pages/tabs/tabs';
import { InicioSesionPage } from '../pages/inicio-sesion/inicio-sesion';
import { RegistroPage } from '../pages/registro/registro';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CuentasProvider } from '../providers/cuentas/cuentas';
import { ContraseñaOlvidadaPage } from '../pages/contraseña-olvidada/contraseña-olvidada';
import { CarroProvider } from '../providers/carro/carro';
import { ProductoProvider } from '../providers/producto/producto';


@NgModule({
  declarations: [
    MyApp,
    RegistroPage,
    InicioSesionPage,
    HomePage,
    ContraseñaOlvidadaPage,
    ActualizarDatosPage,
    DetallesPage,
    CarroDComprasPage,
    PagoPage,
    TabsPage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RegistroPage,
    InicioSesionPage,
    HomePage,
    ContraseñaOlvidadaPage,
    ActualizarDatosPage,
    DetallesPage,
    CarroDComprasPage,
    PagoPage, 
    TabsPage
  ],
  providers: [
    StatusBar,
    HttpClient,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CuentasProvider,
    CarroProvider,
    ProductoProvider
  ]
})
export class AppModule {}
