import { Component } from '@angular/core';

import { InicioSesionPage } from '../inicio-sesion/inicio-sesion';
import { RegistroPage } from './../registro/registro';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = InicioSesionPage;
  tab2Root = RegistroPage;

  constructor() {

  }
}
