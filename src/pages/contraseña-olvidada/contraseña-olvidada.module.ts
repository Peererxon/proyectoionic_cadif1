import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContraseñaOlvidadaPage } from './contraseña-olvidada';

@NgModule({
  declarations: [
    ContraseñaOlvidadaPage,
  ],
  imports: [
    IonicPageModule.forChild(ContraseñaOlvidadaPage),
  ],
})
export class ContraseñaOlvidadaPageModule {}
