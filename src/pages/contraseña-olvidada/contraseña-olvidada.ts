
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { CuentasProvider } from './../../providers/cuentas/cuentas';

@IonicPage()
@Component({
  selector: 'page-contraseña-olvidada',
  templateUrl: 'contraseña-olvidada.html',
})
export class ContraseñaOlvidadaPage {
  correo:string="";
  clave:string="";
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private CuentasProvider:CuentasProvider, private ToastController:ToastController,
              private viewCrtl:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContraseñaOlvidadaPage');
  }

  mostrarToast(mensaje) {
    let toast = this.ToastController.create({
      message: mensaje,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  public buscarUsuario(){
   let encontrado= this.CuentasProvider.buscarPorCorreo(this.correo);
   if (encontrado>-1) {
     this.mostrarToast("Contrasea encontrada!");
     this.clave=this.CuentasProvider.cuentas[encontrado].clave;
    }else{
      this.mostrarToast("El correo ingresado no existe en nuestro sistema");
    }
  }

  cerrar(){
    this.viewCrtl.dismiss();
  }

  

}
