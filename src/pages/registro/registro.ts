import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CuentasProvider } from '../../providers/cuentas/cuentas';

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {
  nombrecompleto:string="";
  correo:string="";
  nombreUsuario:string="";
  clave:string="";
  fecha:string="";
  constructor(public navCtrl: NavController, public navParams: NavParams, private cuentasProvider:CuentasProvider, private toast:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroPage');
  }

  public validarExistencia(){
    if ( this.validarCampos() ) {
      let existente=this.cuentasProvider.buscarPorCorreo( this.correo );
      if ( existente >-1 ) {
        this.mostrarToast("Este correo ya se encuentra registrado")
        }else if( this.cuentasProvider.buscarPorUsuario(this.nombreUsuario) >-1 ){
          this.mostrarToast("Este nombre de usuario se encuentra en uso")
        }else{
          this.mostrarToast("Registro exitoso");
          this.cuentasProvider.agregarCuenta
            (
              this.nombrecompleto.toLocaleLowerCase(),
              this.correo,
              this.nombreUsuario,
              this.clave,
              this.fecha
            );
        }
    }

  }

    public validarCampos(){
      if (this.nombrecompleto==""|| this.nombrecompleto.length<8) {
        this.mostrarToast("Debe ingresar su nombre completo")
        return false
      }else if (this.correo=="") {
          this.mostrarToast("Debe ingresar un correo electronico")
          return false
        }else if (this.nombreUsuario=="") {
            this.mostrarToast("Debe ingresar un nombre de usuario")
            return false
          }else if (this.clave=="") {
              this.mostrarToast("Debe ingresar una clave")
              return false
            }else if (this.fecha=="") {
                this.mostrarToast("Debe ingresar una fecha de nacimiento")
                return false
              }else
                  return true
  }

  mostrarToast(mensaje) {
    let toast = this.toast.create({
      message: mensaje,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}


