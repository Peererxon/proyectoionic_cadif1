import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { CarroProvider } from '../../providers/carro/carro';
import { PagoPage } from '../pago/pago';

/**
 * Generated class for the CarroDComprasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-carro-d-compras',
  templateUrl: 'carro-d-compras.html',
})
export class CarroDComprasPage {
  almacen     :any[] = [];
  total   :number;
  cantidad:number;
  constructor(
    public navCtrl  : NavController,
    public navParams: NavParams,
    public _almacen : CarroProvider,
    public modal    : ModalController,
    ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CarroDComprasPage');
    this.almacen=this._almacen.almacen;
    console.log(this.cantidad);
    if(this._almacen.almacen.length > 0){
      this.total=this.cantidad*this._almacen.almacen[0].precio;
    }
    
  }


   public eliminarDelCarro (id:string){
     this._almacen.eliminarDelCarrito(id);
   }

   public pagar(){
    const modal = this.modal.create(PagoPage);
    modal.present()
   }
}
