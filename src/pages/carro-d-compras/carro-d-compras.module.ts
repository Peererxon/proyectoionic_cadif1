import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarroDComprasPage } from './carro-d-compras';

@NgModule({
  declarations: [
    CarroDComprasPage,
  ],
  imports: [
    IonicPageModule.forChild(CarroDComprasPage),
  ],
})
export class CarroDComprasPageModule {}
