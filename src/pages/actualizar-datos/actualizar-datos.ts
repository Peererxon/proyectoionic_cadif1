import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CuentasProvider } from '../../providers/cuentas/cuentas';

/**
 * Generated class for the ActualizarDatosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-actualizar-datos',
  templateUrl: 'actualizar-datos.html',
})
export class ActualizarDatosPage {
  nombrecompleto:string="";
  correo:string="";
  nombreUsuario:string="";
  clave:string="";
  fecha:string="";
  usuario:{
    nombreCompleto  :"",
    correo  :"",
    nombreUsuario   :"",
    clave           :"",
    fechaDeNacimiento: ""


  };

  posicionUsuario:number;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private toast:ToastController, private cuentasProvider:CuentasProvider) {
                this.usuario        = this.navParams.get("usuario");
                console.log(this.usuario);
                this.nombrecompleto = this.usuario.nombreCompleto;
                this.correo         = this.usuario.correo;
                this.nombreUsuario  = this.usuario.nombreUsuario;
                this.clave          = this.usuario.clave;
                this.fecha          = this.usuario.fechaDeNacimiento;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActualizarDatosPage');
    this.posicionUsuario= this.cuentasProvider.buscarPorUsuario(this.nombreUsuario);
  }

  mostrarToast(mensaje) {
    let toast = this.toast.create({
      message: mensaje,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  public Actualizar(){
    if ( this.validarCampos() ) {
      this.mostrarToast("Actualizacion de datos exitosa!");
      this.cuentasProvider.cuentas[this.posicionUsuario].nombreCompleto=this.nombrecompleto;
      this.cuentasProvider.cuentas[this.posicionUsuario].correo=this.correo;
      this.cuentasProvider.cuentas[this.posicionUsuario].nombreUsuario=this.nombreUsuario;
      this.cuentasProvider.cuentas[this.posicionUsuario].clave=this.clave;
      this.cuentasProvider.cuentas[this.posicionUsuario].fechaDeNacimiento=this.fecha;
      console.log(this.cuentasProvider.cuentas[this.posicionUsuario]);

      console.log(this.usuario);
    }
  }

    public validarCampos(){
      if (this.nombrecompleto==""|| this.nombrecompleto.length<8) {
        this.mostrarToast("Debe ingresar su nombre completo")
        return false
      }else if (this.correo=="") {
          this.mostrarToast("Debe ingresar un correo electronico")
          return false
        }else if (this.nombreUsuario=="") {
            this.mostrarToast("Debe ingresar un nombre de usuario")
            return false
          }else if (this.clave=="") {
              this.mostrarToast("Debe ingresar una clave")
              return false
            }else if (this.fecha=="") {
                this.mostrarToast("Debe ingresar una fecha de nacimiento")
                return false
              }else
                  return true
  }

}
