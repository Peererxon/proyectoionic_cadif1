import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { CarroProvider } from '../../providers/carro/carro';

/**
 * Generated class for the PagoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pago',
  templateUrl: 'pago.html',
})
export class PagoPage {
  cedula          :string = "";
  nombre          :string = "";
  nombreUsuario   :string = "";
  claveTarjeta    :string = "";
  mesDVencimiento :string = "";
  tipoDCuenta     :string = "";
  precio          :string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams,public _toast:ToastController,public almacen : CarroProvider,public viewCrtl:ViewController) {
    this.precio = this.navParams.get("precio");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PagoPage');
    console.log("precioTotal:"+this.precio);
  }

  public comprar(){
    this.mostrarToast("compra exitosa");
    this.almacen.almacen.splice(0,this.almacen.almacen.length);
  }
  cerrar(){
    this.viewCrtl.dismiss();
  }

  mostrarToast(mensaje) {
    let toast = this._toast.create({
      message: mensaje,
      duration: 3000,
      position: 'top'
    });
    toast.present();

  }

}
