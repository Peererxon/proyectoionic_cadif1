import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, ToastController } from 'ionic-angular';
import { CuentasProvider } from '../../providers/cuentas/cuentas';
import { HomePage } from '../home/home';
import { ContraseñaOlvidadaPage } from '../contraseña-olvidada/contraseña-olvidada';


@IonicPage()
@Component({
  selector: 'page-inicio-sesion',
  templateUrl: 'inicio-sesion.html',
})
export class InicioSesionPage {
  nombreUsuario:string="";
  clave:string="";
  constructor(public navCtrl: NavController, private cuentasProvider:CuentasProvider,
    private modalController:ModalController, private toast:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InicioSesionPage');
  }

  public validarUsuario(){
    if (this.nombreUsuario!=""|| this.clave!="") {
      if (this.cuentasProvider.ValidarEntrada(this.nombreUsuario,this.clave)) {
        this.mostrarToast("Iniciando..")
        var usuario=this.cuentasProvider.buscarPorUsuario(this.nombreUsuario);
        this.nombreUsuario="";
        this.clave="";
        console.log(this.cuentasProvider.cuentas[usuario]);
        this.irHome(usuario);
      }else
        this.mostrarToast("Nombre De Usuario o Contraseña Incorrecta")
    }
  }

  public irHome(usuario){
    this.navCtrl.setRoot( HomePage,{"usuario":this.cuentasProvider.cuentas[usuario]} );
  }

  Irperfil(){
    const modal = this.modalController.create(ContraseñaOlvidadaPage);
    modal.present();
  }

  mostrarToast(mensaje) {
    let toast = this.toast.create({
      message: mensaje,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
}
