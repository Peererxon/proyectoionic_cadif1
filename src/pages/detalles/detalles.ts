import { CarroProvider } from './../../providers/carro/carro';
import { ProductoProvider } from './../../providers/producto/producto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { CarroDComprasPage } from '../carro-d-compras/carro-d-compras';
import { PagoPage } from '../pago/pago';


/**
 * Generated class for the DetallesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalles',
  templateUrl: 'detalles.html',
})
export class DetallesPage {
  id                :string;
  productos         :any[] = []; //with filter
  ProductosApi      :any[] = []; //without filter
  ProductosApiFilter:any[] = []; //in the filtering
  
  constructor(public navCtrl:NavController,
     public navParams       :NavParams,
     public _producto       :ProductoProvider,
     public _loading        :LoadingController,
     public ShopCar         :CarroProvider,
     public _toast          :ToastController,
     public _productoService:ProductoProvider,
     public _modalController:ModalController,
     ) {
    this.id = navParams.get("id");
    console.log("id enviado ", this.id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetallesPage');
    let loading=this._loading.create({
      content: "Please wait...",
    });
    
    loading.present();
    console.log("Loading creado");
    let observable = this._productoService.getProductosApi();
    console.log("observable del detalles");
    observable.subscribe( (result:any)=>{
      this.ProductosApi = result;
      this.productos= this.ProductosApi;
      this.ProductosApiFilter= this.ProductosApi;
      filtrado();
      loading.dismiss();
      console.log("Cerrando el loading y filtrado completado");
      console.table(this.productos);
    } )
    let self = this;
   function filtrado(){
    if (self.id!="15") {
      console.log(self.ProductosApi)
      console.log("estas dentro del condicional id<>15")
      console.table("arreglo antes de ser filtrado: "+ self.ProductosApi);
      self.productos =  self.ProductosApiFilter.filter(function(product){
        return product.categories[0].id == parseInt(self.id);
      
      });
      console.log("before of:" +self.productos[0]);

    }else{
      self.productos=self.ProductosApi;
    }
  }

  }

  public addToShopCar(name:string,id:string,$:string,img:string){
    console.log("nombre",name, "id",id, "precio: " , $ ,"imagen: ", img);
    this.ShopCar.agregarAlCarrito(name,id,parseInt($),img);
    console.log(this.ShopCar);
    if(this.ShopCar.almacen.length>0){
      this.mostrarToast("¡producto Agregado a tu carrito!") 
    }
  }

  mostrarToast(mensaje) {
    let toast = this._toast.create({
      message: mensaje,
      duration: 3000,
      position: 'middle'
    });
    toast.present();

  }

  public irPago(){
    const modal = this._modalController.create(PagoPage,{"precio":this.productos[0].precio});
    modal.present()
    
  }
}
