import { CarroDComprasPage } from './../carro-d-compras/carro-d-compras';
import { ProductoProvider } from './../../providers/producto/producto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { InicioSesionPage } from '../inicio-sesion/inicio-sesion';
import { ActualizarDatosPage } from '../actualizar-datos/actualizar-datos';
import { DetallesPage } from '../detalles/detalles';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  usuario      :{};
  n            :number = 0;
  categoriasApi:any[]  = [];

  constructor(
    public navCtrl      : NavController,
    public navParams    : NavParams,
    public _productosApi: ProductoProvider,
    public _loading     : LoadingController,
    public _toast       : ToastController
    ) {
    this.usuario=this.navParams.get("usuario");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');

      let loading=this._loading.create({
        content: "Please wait...",
      });
      
      loading.present();
      let observable = this._productosApi.getCategoriasAPI();
      observable.subscribe( (result:any)=>{
        this.categoriasApi = result;
        loading.dismiss();
      } )

  }

  cerrarSesion(){
    this.navCtrl.setRoot(InicioSesionPage);
  }

  actualizar() {
    this.navCtrl.push(ActualizarDatosPage,{"usuario":this.usuario});
  }

  verDetalle(id:string){
    this.navCtrl.push(DetallesPage,{"id":id})
  }

  Vercarro(){
    this.navCtrl.push(CarroDComprasPage)  
  }

}
